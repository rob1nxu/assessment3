(function(){

    angular
        .module("AnvApp")
        .controller("ProfileCtrl", ["$state", "AuthService","ProfileService", "LogoutSvc", ProfileCtrl]);

    function ProfileCtrl($state, AuthService, ProfileService, LogoutSvc){
        var profilectrl = this;
        
            profilectrl.profilename = "";
            profilectrl.email = "";
            profilectrl.uid = "";
        profilectrl.username = AuthService.Auser;
        console.log("profile username: %s", profilectrl.username);

        // ProfileService.getProfile = profilectrl.username;

       var profileName = profilectrl.username;

       ProfileService.getUserProfile(profileName)
       .then(function(result){
            console.log("ProfileService result is: " + JSON.stringify(result));
            profilectrl.email = result.data.email;
            profilectrl.uid = result.data.uid;

       })
       .catch(function(err){
           console.log("error get user profile");
       })

       
       profilectrl.logout = function(){

        LogoutSvc.logout();

        }
        

        

    }
})();
