(function () {
    angular
        .module("AnvApp")
        .config(anvConfig)
    anvConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function anvConfig($stateProvider, $urlRouterProvider) {

        $stateProvider
        
            .state("signup",{
                url:"/signup",
                templateUrl:"./app/signup/signup.html",
                controller:"SignupCtrl",
                controllerAs:"signupctrl"
            })
            .state("login",{
                url:"/login",
                templateUrl:"./app/login/login.html",
                controller:"LoginCtrl",
                controllerAs:"loginctrl"
            })
            .state("usermain",{
                url:"/usermain",
                templateUrl:"./app/protected/usermain.html",
                resolve: {
                    authenticated: function (AuthService) {
                        console.log("authenticated ?");
                        console.log(AuthService.isUserLoggedIn());
                        return AuthService.isUserLoggedIn();
                    }
                },
                controller:"UmainCtrl",
                controllerAs:"umainctrl"
            })
             .state("profile",{
                url:"/profile",
                templateUrl:"./app/protected/profile.html",
                resolve: {
                    authenticated: function (AuthService) {
                        console.log("authenticated ?");
                        console.log(AuthService.isUserLoggedIn());
                        return AuthService.isUserLoggedIn();
                    }
                },
                controller:"ProfileCtrl",
                controllerAs:"profilectrl"
            })
            .state("postask",{
                url:"/postask",
                templateUrl:"./app/protected/postask.html",
                resolve: {
                    authenticated: function (AuthService) {
                        console.log("authenticated ?");
                        console.log(AuthService.isUserLoggedIn());
                        return AuthService.isUserLoggedIn();
                    }
                },
                controller:"PostaskCtrl",
                controllerAs:"postaskctrl"
            })
            

        $urlRouterProvider.otherwise("/login");
    }
})();