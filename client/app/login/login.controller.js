
(function () {
    angular
        .module("AnvApp")
        .controller("LoginCtrl", ["$state", "AuthService", LoginCtrl]);

    function LoginCtrl($state, AuthService){
        var loginctrl = this;

        // loginctrl.user.username = "";
        // loginctrl.user.password = "";

        loginctrl.inputType = "password"; 

        authenticatedUser = "";


          loginctrl.hideShowPassword = function(){
            if (loginctrl.inputType == 'password')
                loginctrl.inputType = 'text';
            else
                loginctrl.inputType = 'password';
        };


        loginctrl.login = function(){
            AuthService.login(loginctrl.user)
            .then(function(){
                console.log("login");
                
                AuthService.isUserLoggedIn(function(result){
                    if(result){
                        console.log("result is: " + result);
                        authenticatedUser = result; 
                    AuthService.Auser = authenticatedUser;

                        // AuthService.Auser = result;

                     //   $state.go("usermain");
                        $state.go("profile");

                    }else{
                        $state.go("login");
                    }
                })

                //  if(AuthService.isUserLoggedIn()){
                //         loginctrl.user.username = "";
                //         loginctrl.user.password = "";
                //         // vm.progressbar.complete();
                //         $state.go("usermain");
                //     }else{
                //         // Flash.create('danger', "Ooops having issue logging in!", 0, {class: 'custom-class', id: 'custom-id'}, true);
                //         // vm.progressbar.stop();
                //         $state.go("login");
                //     }


            }).catch(function(error){
                console.log("error" + error)
            })
            $state.go("login");
        }
     



    }
})();