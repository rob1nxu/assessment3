"use strict";

// Loads path to access helper functions for working with files and directory paths
var path = require("path");
var express = require("express"); //express is requried here becuase of the express.static here
var config = require("./config");
var bcrypt = require('bcryptjs');

var Users = require("./database").Users;


const LOGIN_PAGE = "/#!/login";

module.exports = function (app, database, passport) {  //this is export the everything below as routesjs as an object that can be passed into express in app.js

    // Defines paths
    // __dirname is a global that holds the directory name of the current module
    const CLIENT_FOLDER = path.join(__dirname + '/../client');
    const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages');


    // MIDDLEWARES --------------------------------------------------------------------------------------------------------
    // Serves files from public directory (in this case CLIENT_FOLDER).
    // __dirname is the absolute path of the application directory.
    // if you have not defined a handler for "/" before this line, server will look for index.html in CLIENT_FOLDER
    app.use(express.static(CLIENT_FOLDER));


    // ROUTE HANDLERS -----------------------------------------------------------------------------------------------------
    // Defines endpoint exposed to client side for registration


    //security starts herer

    //when login keyed and submitted, passport will do the authentication, after that based on success or failure, it will redirect to its respective page. 

    app.post("/register", function (req, res) {
        if (!req.body.password === req.body.confirmpassword) {
            return res.status(500).json({
                err: err
            });
        }
        console.log(req.body.password);
        var hashpassword = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
        console.log("hashpassword: %s ", hashpassword);
        database.connection
            .transaction(function (t) {
                return database.Users
               
                     .create(
                        {
                            username: req.body.username,
                            email: req.body.email,
                            password: hashpassword,
                            fullname: req.body.fullname,
                            contact: req.body.contact
                        }
                    , { transaction: t })
                    .then(function (user) {
                        console.log("what is user: %s", user);
                        console.log("what is user: " + JSON.stringify(user));
                        console.log("user.username is $s", user.username);

                                return database.Userprofile
                                .create(
                                    {
                                        uid: user.uid,
                                        username: req.body.username,
                                        email: req.body.email
                                    }, { transaction: t });
                    });
        
            }).then(function (result) {
                console.log("result is: $s", result);
                res
                    .status(200)
                    .json(result);  

                 }).catch(function (err) {
                console.log(err);
                res
                    .status(501)
                    .json(err);
    });

});

    // function returnResults(results, res) {
    //     res.send(results);
    // };

    app.get("/api/users/userprofile", isAuthenticated, function(req, res){
        database.Userprofile
        .findOne({
            // where: {username: req.body.username}
             where: {username: req.query.queryProfile}
        })
        .then(function(result){
            console.log("SERVERSIDE UserProfile: " + JSON.stringify(result));
            res.json(result);

        })
        .catch(function (err) {
                console.log("ERROR getting UserProfile");

                res
                    .status(500)
                    .json({error: true});
            });
    })



    app.post("/security/login", passport.authenticate("local", { //req n res handled by passport 
        successRedirect: "/#!/usermain",
        failureRedirect: LOGIN_PAGE
    }));

    app.get("/status/user", function (req, res) {
        console.log("executing app.get on SERVERSIDE for /status/user");
        var status = "";
        if (req.user) {
            // status = req.user.email; //assign req.user to status
            //status = req.user.username;
            status =req.user;

        }
        console.log("status is " + JSON.stringify(status));
        // res.status(200).json(status);

        res.send(status).end();
    });

    app.get("/logout", function (req, res) {
        console.log("executing app.get on SERVERSIDE for /logout");

        req.logout();
        req.session.destroy();
        res.send(req.user).end();
    })


    function isAuthenticated(req, res, next) { //check whether person is authenticated before giving them access to protected view
        console.log(">>> " + req.isAuthenticated());
        if (req.isAuthenticated()) {
            console.log(">>>>")
            return next();
        }

        console.log(LOGIN_PAGE);
        res.status(401).json({ "login": false });

    }

    app.use(function (req, res, next) {
        if (req.user == null) {
            res.redirect(LOGIN_PAGE);
        }
        next();
    });


    // Handles 404. In Express, 404 responses are not the result of an error,
    // so the error-handler middleware will not capture them.
    // To handle a 404 response, add a middleware function at the very bottom of the stack
    // (below all other path handlers)
    app.use(function (req, res) {
        res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
    });

    // Error handler: server error
    app.use(function (err, req, res, next) {
        res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
    });

}