// Loads sequelize ORM
var Sequelize = require("sequelize");
var config = require("./config"); //we need the config 


// Defines MySQL configuration
//const MYSQL_USERNAME = 'root';
//const MYSQL_PASSWORD = 'password123';


// DBs, MODELS, and ASSOCIATIONS ---------------------------------------------------------------------------------------
// Creates a MySQL connection
var connection = new Sequelize( config.mysql,
   
    {    
        logging: console.log,
        dialect: 'mysql',   
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);




var Users = require('./models/users')(connection, Sequelize);

var Userprofile = require('./models/userprofile')(connection, Sequelize);

Userprofile.belongsTo(Users, {foreignKey: "uid", targetKey: "uid"});
Userprofile.belongsTo(Users, {foreignKey: "username", targetKey: "username"});
Userprofile.belongsTo(Users, {foreignKey: "email", targetKey: "email"});


module.exports = { // we are exporting out the databasejs for express to link up
    Users: Users,
    Userprofile: Userprofile,
    connection: connection
   
};