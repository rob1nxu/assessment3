
module.exports = function (conn, Sequelize) {
    var Users = conn.define("users",
        {
            uid: {
                type: Sequelize.INTEGER(11),
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            username: {
                type: Sequelize.STRING,
                allowNull: false
            },
            email: {
                type: Sequelize.STRING,
                allowNull: false
            },
            password: {
                type: Sequelize.STRING,
                allowNull: false
            },
            fullname: {
                type: Sequelize.STRING,
                allowNull: false
            },
            contact: {
                type: Sequelize.INTEGER(14),
                allowNull: false
            },
            createdAt: {
                type: Sequelize.DATE,
                allowNull: false
        },
            updatedAt: {
                type: Sequelize.DATE,
                allowNull: false
        }
        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            tableName: "users",
            timestamps: true
         });

    return Users;
};