
module.exports = function (conn, Sequelize) {
    var Userprofile = conn.define("userprofile",
        {
            id: {
                type: Sequelize.INTEGER(11),
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            uid: {
                type: Sequelize.INTEGER(11),
                primaryKey: true,
                allowNull: false
            },
            username: {
                type: Sequelize.STRING,
                primaryKey: true,
                allowNull: false
            },
            email: {
                type: Sequelize.STRING,
                primaryKey: true,
                allowNull: false
            },
            profilename: {
                type: Sequelize.STRING,
                allowNull: true
            },
            profilepic: {
                type: Sequelize.STRING(128),
                allowNull: true
            },
            createdAt: {
                type: Sequelize.DATE,
                allowNull: false
            },
            updatedAt: {
                type: Sequelize.DATE,
                allowNull: false
        }
        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            tableName: "userprofile",
            timestamps: true
         });

    return Userprofile;
};