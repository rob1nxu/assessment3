
USE assessment3db;

CREATE TABLE users (
    uid      INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(14) NOT NULL,
    email VARCHAR(14) NOT NULL,
    password VARCHAR(14) NOT NULL,
    first_name  VARCHAR(14) NOT NULL,
    last_name   VARCHAR(16)     NOT NULL,
    dob  DATE            NOT NULL,
    gender      ENUM ('M','F')  NOT NULL,    
    address VARCHAR(20) NOT NULL,
    contact int(14) NOT NULL,
    
    PRIMARY KEY (uid),
    UNIQUE KEY (username),
	UNIQUE KEY (email)


    
);




CREATE TABLE userprofile (
    id      INT NOT NULL AUTO_INCREMENT,
    userid INT NOT NULL,
    username VARCHAR(14) NOT NULL,
    email VARCHAR(14) NOT NULL,
    profilename  VARCHAR(14),
    profilepic   VARCHAR(128),
    
    FOREIGN KEY (userid) REFERENCES users (uid) ON DELETE CASCADE,
    FOREIGN KEY (username)  REFERENCES users (username)    ON DELETE CASCADE,   
    FOREIGN KEY (email) REFERENCES users (email) ON DELETE CASCADE,
    
    PRIMARY KEY (id)
);


CREATE TABLE postdetails (     id      INT NOT NULL AUTO_INCREMENT,
     postreqId INT NOT NULL,  
     buyrent VARCHAR(14) NOT NULL, 
     type VARCHAR(14) NOT NULL, 
     requirements VARCHAR(14) NOT NULL, 
     location VARCHAR(14) NOT NULL,   
     budget  VARCHAR(16)     NOT NULL,   
     msg VARCHAR(256) NOT NULL,  
     
     PRIMARY KEY (id)
     );

ALTER TABLE postdetails ADD INDEX buyrent(buyrent);



CREATE TABLE postreq (
    id      INT NOT NULL AUTO_INCREMENT,
    userid INT NOT NULL,
	createdon  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    buyrent VARCHAR(14) NOT NULL,
    FOREIGN KEY (userid) REFERENCES userprofile (userid) ON DELETE CASCADE,
	FOREIGN KEY (buyrent) REFERENCES postdetails (buyrent) ON DELETE CASCADE,

    PRIMARY KEY (id)
);



ALTER TABLE postdetails
ADD CONSTRAINT FK_postreqId
FOREIGN KEY (postreqId) REFERENCES postreq (id);








